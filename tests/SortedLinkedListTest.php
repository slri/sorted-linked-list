<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Slri\SortedLinkedList;

final class SortedLinkedListTest extends TestCase
{
    public function testCanAddStringValue(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_STRING);
        $sortedLinkedList->add('s');

        $this->assertSame('i:0;:s:1:"s";', $sortedLinkedList->getList()->serialize());
    }

    public function testCanAddIntegerValue(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_INT);
        $sortedLinkedList->add(1);

        $this->assertSame('i:0;:i:1;', $sortedLinkedList->getList()->serialize());
    }

    public function testCannotAddStringWhenModeIsSetToInteger(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_INT);
        $sortedLinkedList->add('a');
    }

    public function testCannotAddIntegerWhenModeIsSetToString(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_STRING);
        $sortedLinkedList->add(1);
    }

    public function testCanSortIntegerValues(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_INT);
        $sortedLinkedList->add(1);
        $sortedLinkedList->add(6);
        $sortedLinkedList->add(5);
        $sortedLinkedList->add(2);
        $sortedLinkedList->add(2);

        $this->assertSame('i:0;:i:1;:i:2;:i:2;:i:5;:i:6;', $sortedLinkedList->getList()->serialize());
    }

    public function testCanSortStringValues(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_STRING);
        $sortedLinkedList->add('s');
        $sortedLinkedList->add('owo');
        $sortedLinkedList->add('uwu');
        $sortedLinkedList->add('sus');
        $sortedLinkedList->add('aba');

        $this->assertSame('i:0;:s:3:"aba";:s:3:"owo";:s:1:"s";:s:3:"sus";:s:3:"uwu";', $sortedLinkedList->getList()->serialize());
    }

    public function testCanSortIntegerValuesInReverse(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_INT, true);
        $sortedLinkedList->add(1);
        $sortedLinkedList->add(6);
        $sortedLinkedList->add(5);
        $sortedLinkedList->add(2);
        $sortedLinkedList->add(2);

        $this->assertSame('i:0;:i:6;:i:5;:i:2;:i:2;:i:1;', $sortedLinkedList->getList()->serialize());
    }

    public function testCanSortStringValuesInReverse(): void
    {
        $sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_STRING, true);
        $sortedLinkedList->add('s');
        $sortedLinkedList->add('owo');
        $sortedLinkedList->add('uwu');
        $sortedLinkedList->add('sus');
        $sortedLinkedList->add('aba');

        $this->assertSame('i:0;:s:3:"uwu";:s:3:"sus";:s:1:"s";:s:3:"owo";:s:3:"aba";', $sortedLinkedList->getList()->serialize());
    }

    public function testCanAutomaticallySetStringMode(): void
    {
        $sortedLinkedList = new SortedLinkedList();
        $sortedLinkedList->add('s');

        $this->assertSame(SortedLinkedList::MODE_STRING, $sortedLinkedList->getActiveMode());
    }

    public function testCanAutomaticallySetIntegerMode(): void
    {
        $sortedLinkedList = new SortedLinkedList();
        $sortedLinkedList->add(1);

        $this->assertSame(SortedLinkedList::MODE_INT, $sortedLinkedList->getActiveMode());
    }

    public function testCannotAddUnsupportedValue(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $sortedLinkedList = new SortedLinkedList();
        $sortedLinkedList->add([1]);
    }
}