# SortedLinkedList Project for ShipMonk

## Use project as a dependency
### Prerequisites

Add it as a repository to `composer.json` as it's not on packagist:

```json
"require": {
    ...
    "slri/sorted-linked-list": "dev-master",
    ...
},
"repositories": [
    {
        "type": "vcs",
        "url":  "git@gitlab.com:slri/sorted-linked-list.git"
    }
]
```

### Usage

`SortedLinkedList` class keeps the list sorted in either ascending or descending fashion. It only accepts integer or string values. Once a value is added, a mode of string or integer will be set and you'd only be able to add values of the same type. You can explicitly set the mode, otherwise SortedLinkedList will automatically decide which type to accept based on the first value added.

```php
declare(strict_types=1);

require 'vendor/autoload.php';

use Slri\SortedLinkedList;

$sortedLinkedList = new SortedLinkedList(SortedLinkedList::MODE_INT);
$sortedLinkedList->add(1);
$sortedLinkedList->add(3);
$sortedLinkedList->add(6);
$sortedLinkedList->add(5);
$sortedLinkedList->add(2);
$sortedLinkedList->add(2);
```

## Contributing

In order to contribute to the package as a part of another project use `--prefer-source` when installing to pull it as a repository. This allows to make changes and push directly from `vendor` folder. [More info](https://getcomposer.org/doc/03-cli.md#install-i)

### Prerequisites

- composer 2+
- php 8+
- gnu make

1. Run tests: `make test`

2. Run CSFixer: `make pretty`