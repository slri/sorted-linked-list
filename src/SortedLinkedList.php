<?php

declare(strict_types=1);

namespace Slri;

class SortedLinkedList
{
    public const MODE_INT = 'integer';
    public const MODE_STRING = 'string';

    protected \SplDoublyLinkedList $list;

    /**
     * @param bool $reverse Whether to reverse values of linked list
     */
    public function __construct(protected ?string $activeMode = null, protected bool $reverse = false)
    {
        $this->list = new \SplDoublyLinkedList();
    }

    /**
     * Automatically decide whether the linked list will be populated with integer or string values based on the first value.
     *
     * @param mixed $value
     * @return void
     */
    protected function setModeFromValue(mixed $value): void
    {
        if ($this->activeMode) {
            return;
        }

        $type = gettype($value);
        switch ($type) {
            case self::MODE_INT:
                $this->activeMode = self::MODE_INT;

                return;
            case self::MODE_STRING:
                $this->activeMode = self::MODE_STRING;

                return;
            default:
                throw new \InvalidArgumentException(sprintf('Value type of integer or string expected, %1$s given', $type));
        }
    }

    /**
     * Checks if the value data type matches the active mode.
     *
     * @param mixed $value
     * @return boolean
     */
    protected function isValidValue(mixed $value): bool
    {
        $type = gettype($value);

        return self::MODE_INT === $type && self::MODE_INT === $this->activeMode
            || self::MODE_STRING === $type && self::MODE_STRING === $this->activeMode;
    }

    /**
     * Adds a value to a sorted position in the linked list.
     *
     * @param mixed $value
     */
    public function add(mixed $value): void
    {
        $this->setModeFromValue($value);

        if (!$this->isValidValue($value)) {
            throw new \InvalidArgumentException(sprintf('Active mode set to accept values of type %1$s, %2$s given', $this->activeMode, gettype($value)));
        }

        $index = $this->findPositionInOrder($value);
        if (0 === $index) {
            $this->list->unshift($value);

            return;
        }

        if ($index === $this->list->count()) {
            $this->list->push($value);

            return;
        }

        $this->list->add($index, $value);
    }

    /**
     * Returns the index where the value would fit in the sorted list.
     *
     * @param int|string $value
     * @return integer
     */
    protected function findPositionInOrder(int|string $value): int
    {
        if ($this->isEmpty()) {
            return 0;
        }

        $min = 0;
        $max = $this->list->count();
        while ($min < $max) {
            $mid = (int) floor(($min + $max) / 2);
            if ($this->list[$mid] === $value) {
                return $mid;
            }

            if (($this->reverse && ($this->list[$mid] < $value)) || (!$this->reverse && ($this->list[$mid] > $value))) {
                $max = $mid;
                continue;
            }

            $min = $mid + 1;
        }

        return $min;
    }

    /**
     * Returns the current active mode, one of [self::MODE_INT, self::MODE_STRING]
     *
     * @return string
     */
    public function getActiveMode(): string
    {
        return $this->activeMode;
    }

    /**
     * Returns the SplDoublyLinkedList object which is used for linked list functionality underneath.
     *
     * @return \SplDoublyLinkedList
     */
    public function getList(): \SplDoublyLinkedList
    {
        return $this->list;
    }

    /**
     * Removes and returns value from the end of the list.
     *
     * @return integer|string
     */
    public function pop(): int|string
    {
        return $this->list->pop();
    }

    /**
     * Removes and returns value from the beginning of the list.
     *
     * @return integer|string
     */
    public function shift(): int|string
    {
        return $this->list->shift();
    }

    /**
     * Checks whether there are more nodes in the list.
     *
     * @return boolean
     */
    public function valid(): bool
    {
        return $this->list->valid();
    }

    /**
     * Checks whether the list is empty.
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return $this->list->isEmpty();
    }

    /**
     * Returns a list of key value pairs like: [key => value, key => value, ...]
     *
     * @return string
     */
    public function __toString(): string
    {
        $list = '';

        foreach ($this->list as $key => $value) {
            $list .= " {$key} => {$value},";
        }

        return '['.$list.' ]';
    }
}
